Running Locally
===============

You will need to have the D goodies installed, so `dub` and `dmd` and whatnot. You will also need `libsqlite3-dev` installed for working with the output file.

Clone this repository with:
```bash
git clone --recurse-submodules git@gitlab.com:judahnator/emsi-import-project-d.git
```

Then in the directory run `dub run` to run, and `dub build` to build.
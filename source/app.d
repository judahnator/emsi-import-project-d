import d2sqlite3;
import IO.jsonreader;
import Models.soc;
import std.datetime.date;
import std.json;
import std.typecons;
import std.stdio;
import Support.onetmap;
import Support.sochierarchy;
import Support.striphtml;

void main()
{
	// import onet
	OnetMap onetMap = new OnetMap("data/map_onet_soc.csv");

	// import soc-hiearchy
	SocHierarchy socHierarchy = new SocHierarchy("data/soc_hierarchy.csv");

	// database connection and setup table & statement
	Database db = Database("output.sqlite");
	db.run("DROP TABLE IF EXISTS records;
            CREATE TABLE records (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                body TEXT NOT NULL,
                title TEXT NOT NULL,
                expired DATE NOT NULL,
                posted DATE NOT NULL,
                state TEXT NOT NULL,
                city TEXT NOT NULL,
                onet TEXT NOT NULL,
                soc5 TEXT NOT NULL,
                soc2 TEXT NOT NULL
            );");
	Statement query = db.prepare(
		"INSERT INTO records (body, title, expired, posted, state, city, onet, soc5, soc2)
		VALUES (:body, :title, :expired, :posted, :state, :city, :onet, :soc5, :soc2);"
	);

	// Start a database transaction
	db.begin();

	uint importCount = 0;
	ushort tagsPresentIn = 0;

	JsonReader samples = new JsonReader("data/sample.gz");
	foreach (JSONValue sample; samples) {
		string rawBody = sample["body"].str;
		string filteredBody = strip_tags(rawBody);

		if (rawBody != filteredBody) {
			tagsPresentIn++;
		}

		if (!filteredBody) {
			writeln(sample);
			writeln(filteredBody);
			return;
		}

		string expired = sample["expired"].str;
		string posted = sample["posted"].str;
		string onet = sample["onet"].str;

		Soc soc5 = socHierarchy.getRecord(onetMap.getSoc5(onet).get);
		Soc soc2 = socHierarchy.getParentOf(soc5.child, 2);

		query.bind(":body", filteredBody);
		query.bind(":title", sample["title"].str);
		query.bind(":expired", sample["expired"].str);
		query.bind(":posted", sample["posted"].str);
		query.bind(":state", sample["state"].str);
		query.bind(":city", sample["city"].str);
		query.bind(":onet", sample["onet"].str);
		query.bind(":soc5", soc5.child);
		query.bind(":soc2", soc2.child);

		query.execute();
		query.reset();

		// Every 100 rows, commit the results and start a new transaction.
		if ((importCount % 100) == 0) {
			db.commit();
			db.begin();
		}

		importCount++;
	}

	// commit any remaining queries
	db.commit();

	writefln("Records imported: %d", importCount);
	writefln("Documents where HTML was removed: %d", tagsPresentIn);
}

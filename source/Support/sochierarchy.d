module Support.sochierarchy;

import IO.csvreader;
import Models.soc;
import std.conv : to;

final class SocHierarchy {
    private Soc[string] records;

    this(string mapHierarchyFile) {
        CsvReader reader = new CsvReader(mapHierarchyFile);
        foreach (string[string] record; reader) {
            Soc newRecord = { child:record["child"], parent:record["parent"], level:to!ubyte(record["level"]), name:record["name"] };
            records[record["child"]] = newRecord;
        }
    }

    Soc getParentOf(string childSoc, byte level = -1) {
        if (level != -1 && (level < 0 || level > 5)) {
            throw new Exception("The level provided must be 1-5");
        }

        Soc record = getRecord(childSoc);
        do {
            record = getRecord(record.parent);
        } while (level != -1 && record.level > level);

        return record;
    }

    Soc getRecord(string socId) {
        import std.algorithm.mutation : stripRight;
        import std.string : leftJustify;

        if (socId in records) {
            return records[socId];
        }

        string lsn = stripRight(socId[3..$], '0');
        if (lsn == "") {
            throw new Exception("Root level soc has no parent to get.");
        }

        try {
            return getRecord(socId[0..3] ~ leftJustify(lsn[0..($-1)], 4, '0'));
        } catch (Exception e) {
            throw new Exception( "Cannot find soc with id: "~socId, e);
        }
    }
}

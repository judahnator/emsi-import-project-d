module Support.onetmap;

import IO.csvreader;
import std.typecons;

alias NullableString = Nullable!string;

final class OnetMap {
    private string[string] records;

    this(string mapLocation) {
        CsvReader reader = new CsvReader(mapLocation);
        foreach (string[string] record; reader) {
            records[record["onet"]] = record["soc5"];
        }
    }

    public NullableString getSoc5(string onet) {
        NullableString record = NullableString.init;
        if (onet in records) {
            record = records[onet];
        }
        return record;
    }
}

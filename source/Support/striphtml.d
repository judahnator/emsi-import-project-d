module Support.striphtml;

string strip_tags(string input) {
    char[] buffer;
    char lastChar;
    bool inTag = false;
    string output;

    output.reserve(input.length);

    foreach (char c; input) {
        if (inTag) {
            switch (c) {
                case '<':
                    output ~= buffer;
                    buffer = [c];
                    break;
                case '>':
                    buffer = [];
                    inTag = false;
                    break;
                case '/':
                    buffer ~= c;
                    break;
                case ' ':
                    if (lastChar == '<') {
                        output ~= buffer;
                        output ~= c;
                        buffer = [];
                        inTag = false;
                    }
                    break;
                default:
                    buffer ~= c;
                    break;
            }
        } else if (c == '<') {
            buffer ~= c;
            inTag = true;
        } else {
            output ~= c;
        }
        lastChar = c;
    }

    output ~= buffer;

    return output;
}
module IO.filereader;

import IO.Interpreter.interpreter;
import IO.Interpreter.gzinterpreter;
import IO.Interpreter.plaintextinterpreter;
import std.array;

abstract class FileReader (T) {
    protected Interpreter FileInterpreter;

    this(string filePath) {
        import std.array;
        string fileExtension = filePath.split(".")[$ - 1];
        switch (fileExtension) {
            case "csv":
            default:
                FileInterpreter = new PlaintextInterpreter(filePath);
                break;

            case "gz":
                FileInterpreter = new GzInterpreter(filePath);
                break;
        }
    }

    final bool empty() const {
        return FileInterpreter.empty();
    }

    final protected string getFileFront() {
        return FileInterpreter.front();
    }

    abstract T front();

    final void popFront() {
        FileInterpreter.popFront();
    }
}

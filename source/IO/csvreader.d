module IO.csvreader;

import IO.filereader;

alias map = string[string];

final class CsvReader : FileReader!map {
    private bool isHeaderRow = true;
    private string[] headers;

    this(string filePath) {
        super(filePath);
    }

    override map front() {
        import std.array;
        import std.csv;
        import std.range;

        string[] row = this.toStringArray(
            csvReader(this.getFileFront).front()
        );

        if (isHeaderRow) {
            headers = row;
            isHeaderRow = false;
            this.popFront();
            return this.front();
        }

        return assocArray(zip(headers, row));
    }

    private static string[] toStringArray(R)(R record) {
        string[] list = [];
        foreach (string field; record) {
            list ~= field;
        }
        return list;
    }
}

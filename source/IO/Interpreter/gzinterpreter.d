module IO.Interpreter.gzinterpreter;

import IO.Interpreter.interpreter;
import std.algorithm.searching;
import std.ascii;
import std.stdio;
import std.zlib;
import std.typecons;

final class GzInterpreter : Interpreter {
    private UnCompress zipper;
    private string line;
    private char[] buffer;

    this(string filepath) {
        super(filepath);
        zipper = new UnCompress(HeaderFormat.gzip);
        this.popFront;
    }

    override bool empty() const {
        return line == "";
    }

    override string front() {
        import std.ascii;
        import std.string;
        return line.chomp();
    }

    override void popFront() {
        // The old buffer is now the line
        // This is done in case the buffer might happen to contain an entire string with a newline
        auto parsed = parseBuffer(buffer);
        line = parsed[0];
        buffer = parsed[1];

        while (!line.endsWith(newline)) {
            char[] compressedbuffer = file.rawRead(new char[64]);
            if (compressedbuffer.length == 0) {
                break;
            }
            parsed = parseBuffer(buffer ~ cast(char[]) zipper.uncompress(compressedbuffer));
            line ~= parsed[0];
            buffer = parsed[1];
        }
    }

    private static Tuple!(string, char[]) parseBuffer(char[] buffer) {
        string output = "";
        while (buffer.length > 0 && !output.endsWith(newline)) {
            output ~= buffer[0];
            buffer = buffer[1..$];
        }
        return Tuple!(string, char[])(output, buffer);
    }
}

module IO.Interpreter.interpreter;

import std.stdio;
import std.range.interfaces;

abstract class Interpreter {
    protected File file;

    this(string filepath) {
        file = File(filepath, "r");
    }

    ~this() {
        file.close;
    }

    abstract bool empty() const;

    abstract string front();

    abstract void popFront();
}
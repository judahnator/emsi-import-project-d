module IO.Interpreter.plaintextinterpreter;

import IO.Interpreter.interpreter;
import std.file;
import std.range.interfaces;
import std.stdio;
import std.typecons;

final class PlaintextInterpreter : Interpreter {
    private auto thisLine = "";

    this(string filepath) {
        super(filepath);
        this.popFront;
    }

    override bool empty() const {
        return thisLine == null;
    }

    override string front() {
        import std.string;
        return strip(thisLine);
    }

    override void popFront() {
        thisLine = file.readln();
    }
}

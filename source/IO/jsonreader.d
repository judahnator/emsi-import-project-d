module IO.jsonreader;

import IO.filereader;
import std.json;

final class JsonReader: FileReader!JSONValue {
    this(string filePath) {
        super(filePath);
    }

    override JSONValue front() {
        return parseJSON(this.getFileFront());
    }
}

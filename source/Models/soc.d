module Models.soc;

struct Soc {
    string child;
    string parent;
    ubyte level;
    string name;
}